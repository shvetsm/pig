from pig import Player, Inicjalizacja, Game
import unittest
from unittest import mock


class TestPlayer(unittest.TestCase):

	def setUp(self):
		'''Will create new class variables for every next test.'''
		self.player_1 = Player()

	def test_dodaj_do_wyniku(self):
		self.player_1.dodaj_do_wyniku(2)
		self.assertEqual(self.player_1.wynik, 2)

		self.player_1.dodaj_do_wyniku(4)
		self.assertEqual(self.player_1.wynik, 6)

		with self.assertRaises(ValueError):
			self.player_1.dodaj_do_wyniku(-1)

		with self.assertRaises(TypeError):
			self.player_1.dodaj_do_wyniku(2j)

		with self.assertRaises(TypeError):
			self.player_1.dodaj_do_wyniku(True)

		with self.assertRaises(TypeError):
			self.player_1.dodaj_do_wyniku("string")

	def test_dodaj_do_rundy(self):
		self.player_1.dodaj_do_rundy(2)
		self.assertEqual(self.player_1.punkty_rundy, 2)

		with self.assertRaises(ValueError):
			self.player_1.dodaj_do_rundy(0)

		with self.assertRaises((ValueError, TypeError)):
			self.player_1.dodaj_do_rundy(-10.5)

		with self.assertRaises(TypeError):
			self.player_1.dodaj_do_rundy(2j)

		with self.assertRaises(TypeError):
			self.player_1.dodaj_do_rundy(True)

		with self.assertRaises(TypeError):
			self.player_1.dodaj_do_rundy("string")

class Testgra(unittest.TestCase):
	"""docstring for TestInicjalizacja"""

	def setUp(self):
		self.gra = Inicjalizacja()

	def test_number_of_players(self):
		"""If the first three input atempts are inappropriate, raise EOFError. Then shut down"""

		with mock.patch('builtins.input', return_value=1):
			self.gra.number_of_players()
			self.assertEqual(self.gra.liczba_graczy, 1)

		with mock.patch('builtins.input', return_value=-1):
			self.assertRaises(ValueError, self.gra.number_of_players())

	def test_add_players(self):
		#1.
		with mock.patch('builtins.input', return_value=1):
			self.gra.number_of_players()
		with mock.patch('builtins.input', return_value="Max"):
			self.gra.add_players()
			self.assertTrue("Max" in [x.imie for x in self.gra.gracze])

		#2.
		with mock.patch('builtins.input', return_value=1):
			self.gra.number_of_players()	
		with mock.patch('builtins.input', return_value="Komputer"):
			self.assertRaises(NameError, self.gra.add_players())

		#3. Test z więcej niż jednym graczem


class TestGame(unittest.TestCase):
	"""docstring for TestGame"""
	def setUp(self):
		self.gra = Inicjalizacja()
		with mock.patch('builtins.input', return_value=1):
			self.gra.number_of_players()
		with mock.patch('builtins.input', return_value="Max"):
			self.gra.player_set()

	def test_runda(self):
		pass
		"""Find how to input twice or more!!!"""
		#Game.runda(self.gra.gracze[0], ustaw_kostkę_samodzielnie=True)



if __name__ == "__main__":
	unittest.main()