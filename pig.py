import random

class Player:
    """docstring for Player"""
    
    def __init__(self, imie=None, punkty_rundy=0, wstrzymanie=False, wynik=0):
        self.imie = imie
        self.wynik = wynik
        self.punkty_rundy = punkty_rundy
        self.wstrzymanie = wstrzymanie
                              
    def dodaj_do_wyniku(self, runda):
        if runda < 0:
            raise ValueError("Punkty rundy mają postać liczby całkowitej nieujemnej.")
        elif type(runda) != int:
            raise TypeError("Zły format. Punkty rundy należy podać jako liczbę całkowitą.")
        self.wynik += runda
    
    def dodaj_do_rundy(self, punkty):
        if punkty < 1:
            raise ValueError("Punkty kostki mają postać liczby całkowitej dodatniej.")
        elif type(punkty) != int:
            raise TypeError("Zły format. Punkty kostki należy podać jako liczbę całkowitą.")
        self.punkty_rundy += punkty
    
    def resetuj_rundę(self):
        self.punkty_rundy = 0
        self.wstrzymanie = False


class Inicjalizacja:
    
    def __init__(self, l_graczy=1, l_prób=3):
        self.liczba_graczy = l_graczy
        self.gracze = []
        self.liczba_prób = l_prób

    def number_of_players(self):
        liczba_prób = self.liczba_prób
        while liczba_prób != 0:
            try:
                l = int(input("Podaj liczbę graczy:"))
                if l < 1:
                    raise ValueError

            except ValueError:
                liczba_prób -= 1
                if liczba_prób != 0:
                    print("Podaj liczbę całkowitą większą od zera. Pozostało {} prób".format(liczba_prób))
                continue

            else:
                self.liczba_graczy = l
                return
   
        print("Wyczerpano wszystkie proby. Domyślnie gra jedna osoba.")


    def add_players(self):
        liczba_prób = self.liczba_prób
        while liczba_prób != 0:
            try:
                gracz = str(input("Imię gracza {}: ".format(len(self.gracze) + 1)))
                if gracz in [x.imie for x in self.gracze] or gracz == 'Komputer':
                    liczba_prób -= 1
                    raise NameError

            except NameError:
                print("Gracz o takim imieniu już istnieje! Pozostało {} prób".format(liczba_prób))
                continue

            self.gracze.append(Player(imie=gracz))
            return

        print("Wyczerpano wszystkie proby. Imię ustawiono domyślnie na Gracz {}".format(len(self.gracze) + 1))
        self.gracze.append(Player(imie="Gracz {}".format(len(self.gracze) + 1)))

    def player_set(self):
        '''Jeśli jest tylko jeden gracz, to drugi  to komputer'''
        if self.liczba_graczy == 1:
            self.add_players()
            self.gracze.append(Player(imie="Komputer"))
        else:
            for _ in range(1, self.liczba_graczy + 1):
                self.add_players()
        
class Game:
    
    @staticmethod
    def strategia_komputera(gracz):
        if gracz.punkty_rundy >= 20:
            gracz.wstrzymanie = True
        return gracz
    
    @classmethod    
    def runda(cls, gracz, ustaw_kostkę_samodzielnie=False, kostka=None):
        opcje = {'T': True, 'N': False}
        
        while gracz.wstrzymanie == False:

            """This if block needed only for testing"""
            if ustaw_kostkę_samodzielnie == True:
                kostka = int(input("Punkty na kostce:"))
            else:
                kostka = random.randrange(1, 7)
            
            if kostka == 1:
                gracz.resetuj_rundę()
                print("kostka:", kostka, "\t", "runda:", gracz.punkty_rundy, "\t", "razem:", gracz.wynik + gracz.punkty_rundy)
                break
            elif kostka in range(2, 7):
                gracz.dodaj_do_rundy(kostka)
                print("kostka:", kostka, "\t", "runda:", gracz.punkty_rundy, "\t", "razem:", gracz.wynik + gracz.punkty_rundy)
                
                if gracz.imie == 'Komputer':
                    '''Strategia komputera'''
                    cls.strategia_komputera(gracz)    
                else:    
                    while True:
                        try:
                            gracz.wstrzymanie = opcje[str(input("Wstrzymujemy rundę (T/N)?"))]   
                        except KeyError:
                            print("-"*20, "\nWpisz 'T' lub 'N'.", "\n-"*20)
                            continue
                        break
            else:
                raise ValueError("Podana wartość ne może być punktami kostki.")
                            
        gracz.dodaj_do_wyniku(gracz.punkty_rundy)
        gracz.resetuj_rundę()
    
    @classmethod
    def start(cls):
        gra = Inicjalizacja()
        gra.number_of_players()
        gra.player_set()

        while all(x.wynik < 20 for x in gra.gracze): #!!!uwzględniamy możliwość remisu(runda dojdzie do końca nawet jeśli któryś z uczestników osiągnie 100)
            for gracz in gra.gracze:
                print('\n\t{}'.format(gracz.imie))
                cls.runda(gracz)
                
            print("-"*40)
            
        if sum(x.wynik >= 20 for x in gra.gracze) > 1:
            print('\n', '-'*40, '\n\n\t Remis')
            return
        else:
            for gracz in gra.gracze:
                if gracz.wynik >= 20:
                    print('-'*40, '\n\t {} wygrał!'.format(gracz.imie))
                    return
            

if __name__ == "__main__":
    Game.start()